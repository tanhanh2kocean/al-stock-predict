import streamlit as st
from datetime import date
from datetime import datetime
import keras.models as models
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
from plotly import graph_objs as go
import handle_data as hd_data
import xgboost as xgb
import websocket, json
import numpy as np

START = "2015-01-01"
TODAY = date.today().strftime("%Y-%m-%d")

st.title('Stock Forecast App')

currencys = ("BTCUSDT", "ETHUSDT", "BNBUSDT", "SOLUSDT")
selected_currency = st.selectbox('Select coin for prediction', currencys)
time_frame = ('15m', '1h', '4h', '1d')
selected_time_frame = st.selectbox('Time frame', time_frame)
algorithms = ('lstm', 'xgboost')
selected_algorithms = st.selectbox('Algorithms', algorithms)
type = ('close', 'PoC')
selected_type = st.selectbox('Type', type)

limit_candle = 1000
cc = selected_currency[:-1].lower()
interval = '1m'
socket = f'wss://stream.binance.com:9443/ws/{cc}t@kline_{interval}'

raw_data_from_api = hd_data.get_account_info(selected_currency, selected_time_frame, limit_candle)
file_name = "{currency}_{time_frame}_{limit_candle}.csv"
file_name = file_name.format(currency=selected_currency, time_frame=selected_time_frame, limit_candle=limit_candle)
hd_data.save_data_raw_to_csv(raw_data_from_api, file_name)
df = pd.read_csv(file_name)
df["TimeStart"] = pd.to_datetime(df.TimeStart, unit='ms')
df.index = df['TimeStart']
scaler = MinMaxScaler(feature_range=(0, 1))
time_frame = 15;
def calc_time_frame(time_frame):
    if(time_frame=='15m'):
        return 15
    if(time_frame=='1h'):
        return 60
    if(time_frame=='4h'):
        return 60*4
    if(time_frame=='1d'):
        return 60*12

def plot_data(predict, y_value, data, title):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=data['TimeStart'], y=data[y_value], name="stock_open"))
    fig.add_trace(go.Scatter(x=predict['TimeStart'], y=predict['Predictions'], name="stock_predict"))
    fig.layout.update(title_text=title, xaxis_rangeslider_visible=True)
    st.plotly_chart(fig)

placeholder = st.empty()
def on_message(ws, message):
    stock = json.loads(message)
    candle = stock['k']
    placeholder.empty()
    placeholder.write(f"Time start: {datetime.fromtimestamp(candle['t']/1000.0)} | Close price: {candle['c']}")
def on_close(ws):
    print('Connection Closed')
ws = websocket.WebSocketApp(socket, on_message = on_message, on_close= on_close);

if selected_algorithms == 'lstm':
    file_name_model = "{currency}_{time_frame}_{type}_{alg}_model.h5"
    file_name_model = file_name_model.format(currency=selected_currency, time_frame=selected_time_frame,
                                             type=selected_type, alg=selected_algorithms)
    lstm_model = models.load_model(file_name_model)
    new_data = []
    if selected_type == "close":
        new_data = hd_data.convert_data_raw_to_data_predict_closed_price(df)
    if selected_type == "PoC":
        new_data = hd_data.convert_data_raw_to_data_predict_PoC(df)
    new_data.index = new_data.TimeStart
    new_data.drop("TimeStart", axis=1, inplace=True)
    predicted_closing_price = []
    if selected_type == "close":
        predicted_closing_price = hd_data.generate_data_predict_test(new_data, 15, lstm_model, selected_type)
    if selected_type == "PoC":
        predicted_closing_price = hd_data.generate_data_predict_test(new_data, 15, lstm_model, selected_type)
    valid_data = hd_data.generate_data_to_plot(df, predicted_closing_price, calc_time_frame(selected_time_frame))
    st.write(new_data.tail(1))
    st.write(valid_data.iloc[[100]])
    if selected_type == "close":
        plot_data(valid_data, "CloseValue", df, "Predict Stock")
    if selected_type == "PoC":
        new_data = hd_data.convert_data_raw_to_data_predict_PoC(df)
        new_data.index = new_data.TimeStart
        valid_data = hd_data.generate_data_to_plot(new_data, predicted_closing_price, calc_time_frame(selected_time_frame))
        plot_data(valid_data, "PoC", new_data, "Predict Stock")

if selected_algorithms == 'xgboost':
    file_name_model = "{currency}_{time_frame}_{limit_candle}_{type}_{alg}_model.json"
    file_name_model = file_name_model.format(currency=selected_currency, time_frame=selected_time_frame,  limit_candle=1000,
                                             type=selected_type, alg=selected_algorithms)
    model_xgb = xgb.XGBRegressor()
    model_xgb.load_model(file_name_model)

    new_data = []
    if selected_type == "close":
        new_data = hd_data.convert_data_raw_to_data_predict_closed_price(df)
    if selected_type == "PoC":
        new_data = hd_data.convert_data_raw_to_data_predict_PoC(df)
    new_data.index = new_data.TimeStart
    new_data.drop("TimeStart", axis=1, inplace=True)
    predicted_closing_price = []
    if selected_type == "close":
        predicted_closing_price = hd_data.generate_data_predict_PoC(new_data, 15, model_xgb)
    if selected_type == "PoC":
        predicted_closing_price = hd_data.generate_data_predict_PoC(new_data, 15, model_xgb)
    valid_data = hd_data.generate_data_to_plot(df, predicted_closing_price, calc_time_frame(selected_time_frame))
    st.write(new_data.tail(1))
    st.write(valid_data.iloc[[100]])
    if selected_type == "close":
        plot_data(valid_data, "CloseValue", df, "Predict Stock")
    if selected_type == "PoC":
        new_data = hd_data.convert_data_raw_to_data_predict_PoC(df)
        new_data.index = new_data.TimeStart
        valid_data = hd_data.generate_data_to_plot(new_data, predicted_closing_price, calc_time_frame(selected_time_frame))
        plot_data(valid_data, "PoC", new_data, "Predict Stock")

ws.run_forever()
