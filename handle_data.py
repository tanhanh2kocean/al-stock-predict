import json
import requests
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import streamlit as st

api_url_base = 'https://www.binance.com/api/v3/uiKlines?limit={limit_candle}&symbol={currency}&interval={time_frame}'
scaler = MinMaxScaler(feature_range=(0, 1))


def get_account_info(currency, time_frame, limit_candle):
    api_url = api_url_base.format(currency=currency, time_frame=time_frame, limit_candle=limit_candle)
    response = requests.get(api_url)
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None


currency = 'BTCUSDT'
time_frame = '15m'
limit_candle = '100'
account_info = get_account_info(currency, time_frame, limit_candle)


def save_data_raw_to_csv(data, file_name):
    np.savetxt(file_name, data, fmt='%s', comments='', delimiter=',',
               header="TimeStart,OpenValue,MaxValue,MinValue,CloseValue,Unknow1,TimeEnd,Unknow2,Unknow3,Unknow4,Unknow6,Unknow7")


def generate_data_predict_test(data, number, lstm_model, type):
    data_predict = []
    data_raw = data[len(data) - 100 - 60:].values
    i = 0
    data_raw_append_predict = data_raw
    while (i < number + 100):
        inputs_data = data_raw_append_predict
        inputs_data = inputs_data.reshape(-1, 1)
        scaler.fit(inputs_data)
        inputs_data = scaler.transform(inputs_data)
        data_predict.append(inputs_data[i: 60 + i, 0])
        X_test = np.array(data_predict)
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        predicted_closing_price = lstm_model.predict(X_test)
        predicted_closing_price = scaler.inverse_transform(predicted_closing_price)
        i = i + 1
        data_raw_append_predict = np.concatenate((data_raw, np.array(predicted_closing_price).reshape((i, 1))))
    return predicted_closing_price


def convert_data_raw_to_data_predict_closed_price(df):
    new_dataset = pd.DataFrame(index=range(0, len(df)), columns=['TimeStart', "CloseValue"])
    for i in range(0, len(df)):
        new_dataset["TimeStart"][i] = df['TimeStart'][i]
        new_dataset["CloseValue"][i] = df["CloseValue"][i]
    return new_dataset


def convert_data_raw_to_data_predict_PoC(df):
    new_dataset = pd.DataFrame(index=range(1, len(df)), columns=['TimeStart', 'PoC'])
    for i in range(1, len(df)):
        new_dataset["TimeStart"][i] = df['TimeStart'][i]
        new_dataset["PoC"][i] = (df["CloseValue"][i] / df["CloseValue"][i-1] - 1) * 100
    return new_dataset

def generate_data_predict_PoC(data, number, model):
    data_test = []
    data_raw = data[len(data) -100 - 60:].values
    i = 0
    data_raw_append_predict = data_raw
    while (i < number):
        inputs_data1 = data_raw_append_predict
        data_test.append(inputs_data1[i: 60 + i, 0])
        test = np.array(data_test)
        predicted_closing_price = model.predict(test)
        i = i + 1
        data_raw_append_predict = np.concatenate((data_raw, np.array(predicted_closing_price).reshape((i, 1))))
    return data_raw_append_predict


def generate_data_to_plot(df, predicted_closing_price, time_frame):
    next_time_frame = []
    next_time = df["TimeStart"][len(df) -100 - 1].timestamp() * 1000
    for x in predicted_closing_price:
        next_time += time_frame * 60 * 1000
        next_time_frame.append(next_time);
    valid_data = pd.DataFrame({
        "TimeStart": next_time_frame,
        "Predictions": predicted_closing_price.reshape((len(predicted_closing_price),))
    }
    )
    df.index = df['TimeStart']
    valid_data['TimeStart'] = pd.to_datetime(valid_data.TimeStart, unit='ms')
    valid_data.index = valid_data['TimeStart']
    return valid_data
