from keras.models import Sequential
from keras.layers import LSTM,Dropout,Dense
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.model_selection import train_test_split
import handle_data as hd_data

def training_lstm_model(time_frame, currency, limit_candle, type):
    raw_data_from_api = hd_data.get_account_info(currency, time_frame, limit_candle)
    file_name = "{currency}_{time_frame}_{limit_candle}.csv"
    file_name = file_name.format(currency=currency, time_frame=time_frame, limit_candle=limit_candle)
    hd_data.save_data_raw_to_csv(raw_data_from_api, file_name)
    df = pd.read_csv(file_name)
    df["TimeStart"] = pd.to_datetime(df.TimeStart, unit='ms')
    df.index = df['TimeStart']
    df = df.sort_index(ascending=True, axis=0)
    scaler = MinMaxScaler(feature_range=(0, 1))
    new_dataset_with_start_time = []
    if type == "close":
        new_dataset_with_start_time = hd_data.convert_data_raw_to_data_predict_closed_price(df)

    if type == "PoC":
        new_dataset_with_start_time = hd_data.convert_data_raw_to_data_predict_PoC(df)

    print(new_dataset_with_start_time)
    new_dataset = new_dataset_with_start_time
    new_dataset.index = new_dataset.TimeStart
    new_dataset.drop("TimeStart", axis=1, inplace=True)
    final_dataset = new_dataset.values

    # split
    train_data = final_dataset[0:len(df), :]
    scaled_data = scaler.fit_transform(final_dataset)
    x_train_data, y_train_data = [], []
    for i in range(60, len(train_data)):
        x_train_data.append(scaled_data[i - 60:i, 0])
        y_train_data.append(scaled_data[i, 0])

    x_train_data, y_train_data = np.array(x_train_data), np.array(y_train_data)
    x_train_data = np.reshape(x_train_data, (939, 60, 1))
    lstm_model = Sequential()
    lstm_model.add(LSTM(units=50, activation='relu', return_sequences=True, input_shape=(x_train_data.shape[1], 1)))
    lstm_model.add(Dropout(0.3))

    lstm_model.add(LSTM(units=50, activation='relu', return_sequences=True, input_shape=(x_train_data.shape[1], 1)))
    lstm_model.add(Dropout(0.3))

    lstm_model.add(LSTM(units=50))
    lstm_model.add(Dropout(0.3))

    lstm_model.add(Dense(1))

    lstm_model.compile(loss='mean_squared_error', optimizer='adam')
    lstm_model.fit(x_train_data, y_train_data, epochs=5, batch_size=939, verbose=2)

    file_name_model = "{currency}_{time_frame}_{type}_{alg}_model.h5"
    file_name_model = file_name_model.format(currency=currency, time_frame=time_frame, type=type, alg="lstm")
    lstm_model.save(file_name_model)

def traing_xgBoost_model(time_frame, currency, limit_candle, type):
    raw_data_from_api = hd_data.get_account_info(currency, time_frame, limit_candle)
    file_name = "{currency}_{time_frame}_{limit_candle}.csv"
    file_name = file_name.format(currency=currency, time_frame=time_frame, limit_candle=limit_candle)
    hd_data.save_data_raw_to_csv(raw_data_from_api, file_name)
    df = pd.read_csv(file_name)
    df["TimeStart"] = pd.to_datetime(df.TimeStart, unit='ms')
    df.index = df['TimeStart']

    new_dataset_with_start_time = []
    if type == "close":
        new_dataset_with_start_time = hd_data.convert_data_raw_to_data_predict_closed_price(df)

    if type == "PoC":
        new_dataset_with_start_time = hd_data.convert_data_raw_to_data_predict_PoC(df)

    print(new_dataset_with_start_time)
    new_dataset = new_dataset_with_start_time
    new_dataset.index = new_dataset.TimeStart
    new_dataset.drop("TimeStart", axis=1, inplace=True)
    final_dataset = new_dataset.values
    # split
    train_data = final_dataset[0:len(df), :]

    x_train_data, y_train_data = [], []

    for i in range(60, len(train_data)):
        x_train_data.append(train_data[i - 60:i, 0])
        y_train_data.append(train_data[i, 0])

    X_train, X_test, y_train, y_test = train_test_split(x_train_data, y_train_data, test_size=0.3, random_state=0)
    model = xgb.XGBRegressor()
    model.fit(X_train, y_train)
    file_name_model = "{currency}_{time_frame}_{limit_candle}_{type}_{alg}_model.json"
    file_name_model = file_name_model.format(currency=currency, time_frame=time_frame, limit_candle=limit_candle, type=type, alg="xgboost")
    model.save_model(file_name_model)