import streamlit as st
from datetime import date
import keras.models as models
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
from plotly import graph_objs as go
import handle_data as hd_data

START = "2015-01-01"
TODAY = date.today().strftime("%Y-%m-%d")

st.title('Stock Forecast App')

stocks = ('GOOG', 'AAPL', 'MSFT', 'GME')
selected_stock = st.selectbox('Select dataset for prediction', stocks)

n_years = st.slider('Years of prediction:', 1, 4)
period = n_years * 365

df = pd.read_csv("shows.csv")
df["TimeStart"] = pd.to_datetime(df.TimeStart, unit='ms')
df.index = df['TimeStart']
scaler = MinMaxScaler(feature_range=(0, 1))

lstm_model = models.load_model("saved_model_test.h5")

def plot_data(predict):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df['TimeStart'], y=df['CloseValue'], name="stock_open"))
    fig.add_trace(go.Scatter(x=predict['TimeStart'], y=predict['Predictions'], name="stock_predict"))
    fig.layout.update(title_text='Time Series data with Rangeslider', xaxis_rangeslider_visible=True)
    st.plotly_chart(fig)

new_data = hd_data.convert_data_raw_to_data_predict_closed_price(df);
predicted_closing_price = hd_data.generate_data_predict_test(new_data, 100, lstm_model)

valid_data = hd_data.generate_data_to_plot(df, predicted_closing_price, 15);
st.write(valid_data)

plot_data(valid_data)

